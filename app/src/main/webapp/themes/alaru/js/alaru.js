$( document ).ready(function() {
    $("#comment_section").hide();

    $("#comments").click(function () {
        $("#comment_section").slideToggle("fast", function () {
            // Animation complete.
        });
    });

    $( ".commentslink").click(function () {
        if($("#comment_section").length) {
            $("#comment_section").show();
        }
    });

    // When "Comments" link is clicked from home page, we need to show the comments section
    var currentUrl = $(location).attr('href');
    if(currentUrl.indexOf("#comment") != -1) {
        $("#comment_section").show();
    }

    if($("#comment-preview").length) {
        $("#comment_section").show();
        $('html, body').animate({
            scrollTop: $("#comments").offset().top
        }, 100);
    }

    // Need this for off-canvas menu to work
    $(document).foundation();

    // For highlight.js
    hljs.initHighlightingOnLoad();
});