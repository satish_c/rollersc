package org.apache.roller.weblogger.business.sitemap;

import org.apache.roller.weblogger.WebloggerException;
import org.apache.roller.weblogger.business.URLStrategy;
import org.apache.roller.weblogger.business.WeblogEntryManager;
import org.apache.roller.weblogger.business.WebloggerFactory;
import org.apache.roller.weblogger.pojos.*;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * Created by satish on 2/15/15.
 */
public class Sitemap {
    private Weblog weblog;
    private String sitemapPath;
    private String locale;

    private WeblogEntryManager mgr;
    private WeblogEntrySearchCriteria wesc;
    private URLStrategy urlStrategy;
    private SitemapBuilder sitemapBuilder;

    public Sitemap(Weblog weblog, String webappRoot, String locale) {
        this.weblog = weblog;
        this.sitemapPath = webappRoot + "/sitemap.xml";
        this.locale = locale;
    }

    public synchronized void generate() throws WebloggerException, IOException {
        init();

        addEntryUrls();

        addTagUrls();

        addCategoryUrls();

        addFeedUrls();

        writeSitemapFile();
    }

    private void init() {
        mgr = WebloggerFactory.getWeblogger().getWeblogEntryManager();
        wesc = new WeblogEntrySearchCriteria();
        wesc.setWeblog(weblog);
        wesc.setStatus(WeblogEntry.PubStatus.PUBLISHED);

        urlStrategy = WebloggerFactory.getWeblogger().getUrlStrategy();
        String siteHome = urlStrategy.getWeblogCollectionURL(weblog, locale, null, null, null, -1, true);

        sitemapBuilder = new SitemapBuilder();
        sitemapBuilder.addUrl(siteHome);
    }

    private void addEntryUrls() throws WebloggerException {
        List<WeblogEntry> entries = mgr.getWeblogArchives(wesc);
        for(WeblogEntry entry : entries) {
            String entryUrl = urlStrategy.getWeblogEntryURL(weblog, locale, entry.getAnchor(), true);
            sitemapBuilder.addUrl(entryUrl);
        }
    }

    private void addTagUrls() throws WebloggerException {
        List<TagStat> tags = mgr.getTags(weblog, null, null, 0, -1);
        for(TagStat tag : tags) {
            String tagUrl = urlStrategy.getWeblogCollectionURL(weblog, locale, null, null, Arrays.asList(new String[]{tag.getName()}), -1, true);
            sitemapBuilder.addUrl(tagUrl);
        }
    }

    private void addCategoryUrls() throws WebloggerException {
        List<WeblogCategory> categories = mgr.getWeblogCategories(weblog);
        for(WeblogCategory category : categories) {
            String catUrl = urlStrategy.getWeblogCollectionURL(weblog, locale, category.getName(), null, null, -1, true);
            sitemapBuilder.addUrl(catUrl);
        }
    }

    private void addFeedUrls() {
        String feedURL = urlStrategy.getWeblogFeedURL(weblog, locale, "entries", "rss", null, null, null, false, true);
        sitemapBuilder.addUrl(feedURL);
    }

    private void writeSitemapFile() throws IOException {
        File file = new File(sitemapPath);
        sitemapBuilder.write(file);
    }
}