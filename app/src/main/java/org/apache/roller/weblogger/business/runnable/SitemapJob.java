package org.apache.roller.weblogger.business.runnable;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.roller.weblogger.WebloggerException;
import org.apache.roller.weblogger.business.sitemap.Sitemap;
import org.apache.roller.weblogger.pojos.Weblog;

import java.util.Map;

/**
 * Created by satish on 2/15/15.
 */
public class SitemapJob implements Job {
    private static Log log = LogFactory.getLog(SitemapJob.class);

    private Weblog weblog;
    private String locale;
    private String filePath;

    public SitemapJob(Weblog weblog, String filePath, String locale) {
        this.weblog = weblog;
        this.filePath = filePath;
        this.locale = locale;
    }

    @Override
    public void execute() {
        Sitemap sitemap = new Sitemap(weblog, filePath, locale);
        try {
            log.debug(filePath);
            sitemap.generate();
        } catch (Exception e) {
            log.warn("Sitemap generation failed", e);
        }
    }

    @Override
    public void input(Map<String, Object> input) { }

    @Override
    public Map<String, Object> output() {
        return null;
    }
}
