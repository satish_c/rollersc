/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 *  contributor license agreements.  The ASF licenses this file to You
 * under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.  For additional information regarding
 * copyright in this work, please see the NOTICE file in the top level
 * directory of this distribution.
 */

package org.apache.roller.weblogger.business;

import org.apache.roller.weblogger.pojos.Weblog;

import java.util.List;
import java.util.Map;

/**
 * Created by satish on 1/13/15.
 */
public class HttpsUrlStrategy implements URLStrategy {
    private URLStrategy baseURLStrategy;

    public HttpsUrlStrategy(URLStrategy baseURLStrategy) {
        this.baseURLStrategy = baseURLStrategy;
    }

    private String getHttpsUrl(String url) {
        if(url.startsWith("http://")) {
            url = url.replaceFirst("^http", "https");
        }
        return url;
    }

    @Override
    public URLStrategy getPreviewURLStrategy(String previewTheme) {
        return baseURLStrategy.getPreviewURLStrategy(previewTheme);
    }

    @Override
    public String getLoginURL(boolean absolute) {
        String url = baseURLStrategy.getLoginURL(absolute);
        return getHttpsUrl(url);
    }

    @Override
    public String getLogoutURL(boolean absolute) {
        String url = baseURLStrategy.getLogoutURL(absolute);
        return getHttpsUrl(url);
    }

    @Override
    public String getRegisterURL(boolean absolute) {
        String url = baseURLStrategy.getRegisterURL(absolute);
        return getHttpsUrl(url);
    }

    @Override
    public String getActionURL(String action, String namespace, String weblogHandle, Map<String, String> parameters, boolean absolute) {
        String url = baseURLStrategy.getActionURL(action, namespace, weblogHandle, parameters, absolute);
        return getHttpsUrl(url);
    }

    @Override
    public String getEntryAddURL(String weblogHandle, boolean absolute) {
        String url = baseURLStrategy.getEntryAddURL(weblogHandle, absolute);
        return getHttpsUrl(url);
    }

    @Override
    public String getEntryEditURL(String weblogHandle, String entryId, boolean absolute) {
        String url = baseURLStrategy.getEntryEditURL(weblogHandle, entryId, absolute);
        return getHttpsUrl(url);
    }

    @Override
    public String getWeblogConfigURL(String weblogHandle, boolean absolute) {
        String url = baseURLStrategy.getWeblogConfigURL(weblogHandle, absolute);
        return getHttpsUrl(url);
    }

    @Override
    public String getOpenSearchSiteURL() {
        String url = baseURLStrategy.getOpenSearchSiteURL();
        return getHttpsUrl(url);
    }

    @Override
    public String getOpenSearchWeblogURL(String weblogHandle) {
        String url = baseURLStrategy.getOpenSearchWeblogURL(weblogHandle);
        return getHttpsUrl(url);
    }

    @Override
    public String getWeblogSearchFeedURLTemplate(Weblog weblog) {
        String url = baseURLStrategy.getWeblogSearchFeedURLTemplate(weblog);
        return getHttpsUrl(url);
    }

    @Override
    public String getWeblogSearchPageURLTemplate(Weblog weblog) {
        String url = baseURLStrategy.getWeblogSearchPageURLTemplate(weblog);
        return getHttpsUrl(url);
    }

    @Override
    public String getXmlrpcURL(boolean absolute) {
        String url = baseURLStrategy.getXmlrpcURL(absolute);
        return getHttpsUrl(url);
    }

    @Override
    public String getAtomProtocolURL(boolean absolute) {
        String url = baseURLStrategy.getAtomProtocolURL(absolute);
        return getHttpsUrl(url);
    }

    @Override
    public String getWeblogURL(Weblog weblog, String locale, boolean absolute) {
        String url = baseURLStrategy.getWeblogURL(weblog, locale, absolute);
        return getHttpsUrl(url);
    }

    @Override
    public String getWeblogEntryURL(Weblog weblog, String locale, String entryAnchor, boolean absolute) {
        String url = baseURLStrategy.getWeblogEntryURL(weblog, locale, entryAnchor, absolute);
        return getHttpsUrl(url);
    }

    @Override
    public String getWeblogCommentsURL(Weblog weblog, String locale, String entryAnchor, boolean absolute) {
        String url = baseURLStrategy.getWeblogCommentsURL(weblog, locale, entryAnchor, absolute);
        return getHttpsUrl(url);
    }

    @Override
    public String getWeblogCommentURL(Weblog weblog, String locale, String entryAnchor, String timeStamp, boolean absolute) {
        String url = baseURLStrategy.getWeblogCommentURL(weblog, locale, entryAnchor, timeStamp, absolute);
        return getHttpsUrl(url);
    }

    @Override
    public String getMediaFileURL(Weblog weblog, String fileAnchor, boolean absolute) {
        String url = baseURLStrategy.getMediaFileURL(weblog, fileAnchor, absolute);
        return getHttpsUrl(url);
    }

    @Override
    public String getMediaFileThumbnailURL(Weblog weblog, String fileAnchor, boolean absolute) {
        String url = baseURLStrategy.getMediaFileThumbnailURL(weblog, fileAnchor, absolute);
        return getHttpsUrl(url);
    }

    @Override
    public String getWeblogCollectionURL(Weblog weblog, String locale, String category, String dateString, List tags, int pageNum, boolean absolute) {
        String url = baseURLStrategy.getWeblogCollectionURL(weblog, locale, category, dateString, tags, pageNum, absolute);
        return getHttpsUrl(url);
    }

    @Override
    public String getWeblogPageURL(Weblog weblog, String locale, String pageLink, String entryAnchor, String category, String dateString, List tags, int pageNum, boolean absolute) {
        String url = baseURLStrategy.getWeblogPageURL(weblog, locale, pageLink, entryAnchor, category, dateString, tags, pageNum, absolute);
        return getHttpsUrl(url);
    }

    @Override
    public String getWeblogFeedURL(Weblog weblog, String locale, String type, String format, String category, String term, List tags, boolean excerpts, boolean absolute) {
        String url = baseURLStrategy.getWeblogFeedURL(weblog, locale, type, format, category, term, tags, excerpts, absolute);
        return getHttpsUrl(url);
    }

    @Override
    public String getWeblogSearchURL(Weblog weblog, String locale, String query, String category, int pageNum, boolean absolute) {
        String url = baseURLStrategy.getWeblogSearchURL(weblog, locale, query, category, pageNum, absolute);
        return getHttpsUrl(url);
    }

    @Override
    public String getWeblogResourceURL(Weblog weblog, String filePath, boolean absolute) {
        String url = baseURLStrategy.getWeblogResourceURL(weblog, filePath, absolute);
        return getHttpsUrl(url);
    }

    @Override
    public String getWeblogRsdURL(Weblog weblog, boolean absolute) {
        String url = baseURLStrategy.getWeblogRsdURL(weblog, absolute);
        return getHttpsUrl(url);
    }

    @Override
    public String getWeblogTagsJsonURL(Weblog weblog, boolean absolute, int pageNum) {
        String url = baseURLStrategy.getWeblogTagsJsonURL(weblog, absolute, pageNum);
        return getHttpsUrl(url);
    }

    @Override
    public String getOAuthRequestTokenURL() {
        String url = baseURLStrategy.getOAuthRequestTokenURL();
        return getHttpsUrl(url);
    }

    @Override
    public String getOAuthAuthorizationURL() {
        String url = baseURLStrategy.getOAuthAuthorizationURL();
        return getHttpsUrl(url);
    }

    @Override
    public String getOAuthAccessTokenURL() {
        String url = baseURLStrategy.getOAuthAccessTokenURL();
        return getHttpsUrl(url);
    }
}
