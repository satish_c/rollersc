package org.apache.roller.weblogger.pojos;

import java.io.Serializable;

/**
 * Created by satish on 2/7/15.
 */
public class ArchiveEntry implements Serializable {
    private static final long serialVersionUID = -865141526804317995L;
    private String displayDate;
    private String date;
    private Integer entriesCount;

    public String getDisplayDate() {
        return displayDate;
    }

    public void setDisplayDate(String displayDate) {
        this.displayDate = displayDate;
    }

    public Integer getEntriesCount() {
        return entriesCount;
    }

    public void setEntriesCount(Integer entriesCount) {
        this.entriesCount = entriesCount;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
