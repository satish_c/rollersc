var gulp = require('gulp');
var sass = require('gulp-sass');
var rename = require('gulp-rename');

gulp.task('styles', function() {
    gulp.src('roller.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(rename('app.css'))
        .pipe(gulp.dest('../app/src/main/webapp/themes/alaru/foundation/'))
});

//Watch task
gulp.task('default',function() {
    gulp.watch('*.scss',['styles']);
});