This is a customized version of [Apache Roller](http://roller.apache.org/). The customizations include:

* A new minimalist and responsive theme - Alaru
* Limited support for generating sitemaps. Currently can be done only from the admin main menu and needs to be done manually each time a new post is published.
* Larger permalink length.

These customizations can be viewed on my blog [satishchilukuri.com](https://satishchilukuri.com/blog/)

To get the source code, clone this repository:

`git clone https://bitbucket.org/satish_c/roller.git`

To build the source, refer to Roller's [README.txt](https://bitbucket.org/satish_c/rollersc/src/27220402d2c393b412dfa5324e33e912bb70c6e6/README.txt?at=master).